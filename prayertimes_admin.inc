<?php

function prayertimes_admin() {
	drupal_add_css(drupal_get_path('module', 'prayertimes') . '/css/prayertimes_admin.css');
	drupal_add_js(drupal_get_path('module', 'prayertimes') . '/js/prayertimes.js');
  $settings = _prayertimes_get_settings();
  
  $form['location'] = array(
    '#type' => 'fieldset',
    '#title' => t('Location based settings'),
    '#weight' => -5,
    '#collapsible' => TRUE,
    '#collapsed' => False,
  );
  
  $form['location']['gmap_api_key'] = array(
		'#type' => 'textfield',
		'#title' => t('Google Maps API Key'),
		'#default_value' => $settings['gmap_api_key'],
		'#size' => 40,
		'#maxlength' => 255,
		'#description' => t('Your personal Googlemaps API key. You must get this for each separate website at <a href="http://www.google.com/apis/maps/">Google Map API website</a>.'),
    '#required' => TRUE,
  );
  
  $form['location']['location_name'] = array(
    '#prefix' => '<div class="prayertimes-location-settings">',
    '#type' => 'textfield',
    '#title' => t('Enter City, State or ZIP code'),
    '#description' => t(''),
    '#default_value' => $settings['default_location']['name'],
    '#size' => 40,
    '#maxlength' => 20,
    '#required' => TRUE,
  );
  
  $form['location']['location_set'] = array(
    '#type' => 'button',
    '#value' => t('Set'),
    '#validate' => array('_prayertimes_validate_location'),
    '#suffix' => "<div class=\"prayertimes-clearfix\"></div>\n</div>",
  );
  
  $form['location']['location_lat'] = array(
    '#type' => 'hidden',
    '#title' => t('Latitude'),
    '#default_value' => $settings['default_location']['latitude'],
  );
  
  $form['location']['location_long'] = array(
    '#type' => 'hidden',
    '#title' => t('Longitude'),
    '#default_value' => $settings['default_location']['longitude'],
  );
  
  $form['prayer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Prayer times settings'),
    '#weight' => -4,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['prayer']['madhab'] = array(
    '#type' => 'radios',
    '#title' => t('Madhab'),
    '#default_value' => $settings['madhab'],
    '#description' => t('Asr calculation opinion.'),
    '#options' => array(
      0 => t("Shafi'i, Maliki, and Hanbali"), 
      1 => t('Hanafi'), 
    ),
  );
  
  $form['prayer']['method'] = array(
    '#type' => 'select',
    '#title' => t('Calculation method'),
    '#default_value' => $settings['method'],
    '#description' => t('The calculation methods specify a Sunrise angle and a Sunset angle which allow to correctly calculate your Fajr and Isha times.'),
    '#options' => array(
      0 => t('Ithna Ashari'),
      1 => t('University of Islamic Sciences, Karachi'),
      2 => t('Islamic Society of North America (ISNA)'),
      3 => t('Muslim World League (MWL)'),
      4 => t('Umm al-Qura, Makkah'),
      5 => t('Egyptian General Authority of Survey'),
      6 => t('Institute of Geophysics, University of Tehran'),
      7 => t('Custom'), 
    ),
  );
  
  $rebuild = FALSE;
  if (!empty($form_state['rebuild'])) {
    $rebuild = $form_state['rebuild'];
  }
  if ($rebuild == TRUE) {
    // Trigger recursive state restore. Remember, our values overwritten in $form_state would be used for appropriate fields.
    take_control_restore_user_input($form, $form_state);
    $form_state['rebuild'] = FALSE;
  }  
  
  $form['buttons']['submit'] = array('#type' => 'submit', '#weight' => 10, '#value' => t('Save configuration') );
  $form['#theme'] = 'system_settings_form';
  $form['#submit'][] = 'prayertimes_admin_submit';
  
  return $form;
}

function _prayertimes_validate_location(&$form = null, &$form_state = null) {
  if ($_REQUEST['location_name_js']) { // This tests if the request was made by JavaScript AJAX callback
  	$settings = _prayertimes_get_settings();
    $location_name = drupal_urlencode($_REQUEST['location_name_js']);
    $location_data = drupal_http_request("http://maps.google.com/maps/geo?q=$location_name&output=json&oe=utf8&sensor=true_or_false&key=" . $settings['gmap_api_key'] ."");
    print drupal_to_js($location_data->data);
  }
  else { // Here comes the regular form submit for setting longitude and latitude 
    $location_name = drupal_urlencode($form_state['values']['location_name']);
    $location_data = drupal_http_request("http://maps.google.com/maps/geo?q=$location_name&output=json&oe=utf8&sensor=true_or_false&key=" . $form_state['values']['gmap_api_key'] ."");
    $location_data = json_decode($location_data->data);
    form_set_value($form['location']['location_name'], $location_data->Placemark[0]->address, $form_state);
    form_set_value($form['location']['location_lat'], $location_data->Placemark[0]->coordinates[1], $form_state);
    form_set_value($form['location']['location_long'], $location_data->Placemark[0]->coordinates[0], $form_state);
  }
}

function prayertimes_admin_submit($form, &$form_state) {
  $settings = array(
    'default_location' => array('name' => $form_state['values']['location_name'], 'latitude' => $form_state['values']['location_lat'], 'longitude' => $form_state['values']['location_long']),
    'gmap_api_key' => $form_state['values']['gmap_api_key'],
  );
  variable_set('prayertimes', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
}