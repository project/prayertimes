
if (Drupal.jsEnabled) {	
	Drupal.behaviors.prayerTimes = function (context) {
		$('#edit-location-set').ajaxStart( function () {
			$(this).attr('disabled', 'disabled');
			$('#edit-location-name').addClass('prayertimes-loading');
		}).ajaxStop( function () {
			$('#edit-location-name').removeClass('prayertimes-loading');
			$(this).removeAttr("disabled");
		});
		
		$('#edit-location-set').click(function() {
			var target = $('#edit-location-name');
			var latitude = $('#edit-location-lat');
			var longitude = $('#edit-location-long');
			$.ajax({
				type: 'POST',
				data: 'location_name_js=' + target.val(),
				dataType: 'json',
				url: Drupal.settings.basePath + 'prayertimes/validate_location',
				success: function(response){
					var result = JSON.parse(response);
					if (result.Placemark) {
						if (result.Placemark.length != 0) {
							target.val(result.Placemark[0].address);
							latitude.val(result.Placemark[0].Point.coordinates[1]);
							longitude.val(result.Placemark[0].Point.coordinates[0]);
						}
					}
					else {
						alert(Drupal.t('There was an error processing you location. Please make sure you have typed a valid location or that you have provided a valid Google Maps API Key.'));
					}
				}
			});
		return false;
		});

	}
}